import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Container,
  Box,
  Card,
  CardActions,
  CardContent,
  Button,
  Typography,
  TextField,
  Checkbox,
  Switch,
} from "@material-ui/core/";
import DeleteIcon from "@material-ui/icons/Delete";
import moment from "moment";

const useStyles = makeStyles({
  wrapper: {
    background: "#22242b",
    height: "100%",
    boxShadow: "0px 0px 30px 50px #22242b",
  },
  list: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
  },
  task: {
    width: "100%",
    marginTop: "5px",
    backgroundColor: "#5DA9E9",
    display: "flex",
    boxShadow: "0px 2px black",
  },
  add: {
    backgroundColor: "#fff",
    margin: "10px 0px",
    width: "100%",
    display: "flex",
    boxShadow: "0px 2px black",
  },
  addTextField: {
    width: "100%",
    backgroundColor: "#5DA9E9",
    color: "#000",
  },
  checked: {
    textDecoration: "line-through",
    color: "#003F91",
  },
  filters: {
    display: "flex",
    alignItems: "center",
    background: "#5DA9E9",
    margin: "10px",
    padding: "0px 0px 0px 12px",
    borderRadius: "5px",
    boxShadow: "0px 2px black",
  },
});

//Todo task
interface TaskProps {
  id: number;
  value: string;
  date: string;
  isChecked: boolean;
  callback: (action: string, id: number) => void;
}

const Task: React.FC<TaskProps> = (props) => {
  const { id, value, date, isChecked, callback } = props;
  const classes = useStyles();

  return (
    <Card className={classes.task}>
      <Checkbox
        checked={isChecked}
        inputProps={{ "aria-label": "primary checkbox" }}
        color="primary"
        onClick={() => {
          callback("done", id);
        }}
      />
      <CardContent style={{ width: "100%", wordBreak: "break-all" }}>
        <Typography
          style={{ fontSize: "calc(1rem - 5px)" }}
          className={isChecked ? classes.checked : ""}
        >
          {date}
        </Typography>
        <Typography className={isChecked ? classes.checked : ""}>
          {value}
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          style={{ width: "50%" }}
          onClick={() => {
            callback("delete", id);
          }}
        >
          <DeleteIcon />
        </Button>
      </CardActions>
    </Card>
  );
};

//Todo list
const Todo: React.FC<any> = () => {
  const classes = useStyles();
  const [tasks, setTasks] = React.useState([
    { id: 1, value: "My task", date: "01-01-2021", isChecked: true },
    { id: 2, value: "My task", date: "01-01-2020", isChecked: false },
    { id: 3, value: "My task", date: "01-01-2019", isChecked: false },
  ]);
  const [taskKeys, setTaskKeys] = React.useState([1, 2, 3]);
  const [newTask, setNewTask] = React.useState("");
  const [showFinished, setShowFinished] = React.useState(false);

  useEffect(() => {
    let finishedTasks = tasks.filter((item) => item.isChecked === true);
    console.log("Checked Elements:", finishedTasks);
  }, [tasks]);

  //input handler for new tasks
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.id === "inputTask") {
      setNewTask(event.target.value);
    }
    if (event.target.name === "switch") {
      setShowFinished(!showFinished);
    }
  };

  //logic for adding new task
  const add = () => {
    let dateNow = moment().format("DD-MM-YYYY");
    let idNumber = taskKeys.length + 1;
    setTaskKeys([...taskKeys, idNumber]); //created this in order to get rid of the warnings due to duplicated keys
    setTasks([
      ...tasks,
      { id: idNumber, value: newTask, date: dateNow, isChecked: false },
    ]);
    setNewTask("");
  };

  //logic for removed task
  const remove = (removedTask: number) => {
    let filteredTasks = tasks.filter((item) => item.id !== removedTask);
    setTasks(filteredTasks);
  };

  //logic for done task
  const done = (doneTask: number) => {
    let modifiedTasks = tasks.map((item) => {
      if (item.id === doneTask) {
        let value = !item.isChecked;
        item.isChecked = value;
      }
      return item;
    });
    setTasks(modifiedTasks);
  };

  //logic for callback inside task components
  const reducer = (action: string, id: number) => {
    if (action === "done") {
      done(id);
    } else if (action === "delete") {
      remove(id);
    }
  };

  return (
    <Container className={classes.wrapper} maxWidth="sm">
      <Box className={classes.list}>
        <Box className={classes.add}>
          <TextField
            id="inputTask"
            className={classes.addTextField}
            onChange={handleChange}
            color="primary"
            label="Add new task"
            variant="filled"
            value={newTask}
          />
          <Button
            style={{ width: "20%", borderRadius: "0px" }}
            color="primary"
            variant="contained"
            onClick={() => {
              add();
            }}
          >
            Add
          </Button>
        </Box>
        <Box className={classes.filters}>
          <Typography>Show Finished</Typography>
          <Switch
            checked={showFinished}
            onChange={handleChange}
            name="switch"
            color="primary"
          />
        </Box>
        {tasks.map((ele) => {
          if (showFinished) {
            if (ele.isChecked === true) {
              return (
                <Task
                  key={"task" + ele.id}
                  id={ele.id}
                  value={ele.value}
                  date={ele.date}
                  isChecked={ele.isChecked}
                  callback={reducer}
                />
              );
            } else return "";
          } else {
            return (
              <Task
                key={"task" + ele.id}
                id={ele.id}
                value={ele.value}
                date={ele.date}
                isChecked={ele.isChecked}
                callback={reducer}
              />
            );
          }
        })}
      </Box>
    </Container>
  );
};

export default Todo;
